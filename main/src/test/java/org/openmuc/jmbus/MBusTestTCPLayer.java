package org.openmuc.jmbus;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import com.fazecast.jSerialComm.SerialPort;
import org.openmuc.jmbus.transportlayer.TransportLayer;

public class MBusTestTCPLayer implements TransportLayer {

    private final DataInputStream is;
    private final DataOutputStream os;
    private boolean closed = true;
    private int timeout = 1000;

    public MBusTestTCPLayer(DataInputStream is, DataOutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public SerialPort getSerialPort() {
        throw new IllegalCallerException("TCP Layer has no serial functionality");
    }

    @Override
    public void open() {
        closed = false;
    }

    @Override
    public void close() {
        closed = true;
    }

    @Override
    public DataOutputStream getOutputStream() {
        return os;
    }

    @Override
    public DataInputStream getInputStream() {
        return is;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public void setTimeout_in_ms(int timeout_in_ms) {
        this.timeout = timeout_in_ms;
    }

    @Override
    public int getTimeout_in_ms() {
        return timeout;
    }

}

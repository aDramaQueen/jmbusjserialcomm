/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.openmuc.jmbus.transportlayer;

import com.fazecast.jSerialComm.SerialPort;

/**
 * Connection builder for serial connections.
 */
public abstract class SerialBuilder<T, S extends SerialBuilder<T, S>> extends Builder<T, S> {

    private final SerialPort serialPort;
    private final String serialPortName;

    private int baudrate;
    private int dataBits;
    private int stopBits;
    private int parity;

    protected SerialBuilder(String serialPortName) {
        this.serialPort = SerialPort.getCommPort(serialPortName);
        this.serialPortName = serialPortName;

        this.baudrate = 2400;
        this.dataBits = 8;
        this.stopBits = SerialPort.ONE_STOP_BIT;
        this.parity = SerialPort.EVEN_PARITY;
    }

    public SerialPort getSerialPort(){
        return this.serialPort;
    }

    public S setBaudrate(int baudrate) {
        this.baudrate = baudrate;
        return self();
    }

    public S setDataBits(int dataBits) {
        this.dataBits = dataBits;
        return self();
    }

    public S setStopBits(int stopBits) {
        this.stopBits = stopBits;
        return self();
    }

    public S setParity(int parity) {
        this.parity = parity;
        return self();
    }

    @Override
    protected TransportLayer buildTransportLayer() {
        SerialPort currentPort = SerialPort.getCommPort(serialPortName);
        currentPort.setComPortParameters(baudrate, dataBits, stopBits, parity, false);
        currentPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, getTimeout(), Constants.STANDARD_WRITING_TIMEOUT_IN_MS.time_in_ms);
        return new Connector(currentPort);
    }

}

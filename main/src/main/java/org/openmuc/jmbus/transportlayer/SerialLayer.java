/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package org.openmuc.jmbus.transportlayer;

import com.fazecast.jSerialComm.SerialPort;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

class SerialLayer implements TransportLayer {

    private final SerialPort serialPort;
    private int timeout_in_ms;

    private DataOutputStream os;
    private DataInputStream is;

    public SerialLayer(SerialPort serialPort) {
        this.serialPort = serialPort;
        this.timeout_in_ms = Constants.STANDARD_READING_TIMEOUT_IN_MS.time_in_ms;
    }

    public SerialLayer(int timeout_in_ms, SerialPort serialPort) {
        this.serialPort = serialPort;
        this.timeout_in_ms = timeout_in_ms;
    }

    public boolean setTimeOut(int timeout_in_ms){
        this.timeout_in_ms = timeout_in_ms;
        return serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, timeout_in_ms, Constants.STANDARD_WRITING_TIMEOUT_IN_MS.time_in_ms);
    }

    @Override
    public SerialPort getSerialPort() {
        return serialPort;
    }

    @Override
    public void open() throws IOException {
        serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, timeout_in_ms, Constants.STANDARD_WRITING_TIMEOUT_IN_MS.time_in_ms);

        os = new DataOutputStream(serialPort.getOutputStream());
        is = new DataInputStream(serialPort.getInputStream());
    }

    @Override
    public DataOutputStream getOutputStream() {
        return os;
    }

    @Override
    public DataInputStream getInputStream() {
        return is;
    }

    @Override
    public void close() {
        if (serialPort == null || !serialPort.isOpen()) {
            return;
        }
        serialPort.closePort();
    }

    @Override
    public boolean isClosed() {
        return !serialPort.isOpen();
    }

    @Override
    public void setTimeout_in_ms(int timeout_in_ms) {
        serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, timeout_in_ms, Constants.STANDARD_WRITING_TIMEOUT_IN_MS.time_in_ms);
    }

    @Override
    public int getTimeout_in_ms() {
        return serialPort.getReadTimeout();
    }

}

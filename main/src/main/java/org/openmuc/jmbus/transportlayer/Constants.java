package org.openmuc.jmbus.transportlayer;

public enum Constants {

    //ATTENTION: STANDARD_WRITING_TIMEOUT JUST FOR WINDOWS USEFUL
    STANDARD_WRITING_TIMEOUT_IN_MS(250),
    STANDARD_READING_TIMEOUT_IN_MS(250);

    public final int time_in_ms;

    Constants(int time_in_ms){
        this.time_in_ms = time_in_ms;
    }
}

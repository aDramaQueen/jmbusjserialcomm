package org.openmuc.jmbus.transportlayer;

import com.fazecast.jSerialComm.SerialPort;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 * Utility class for translation between jSerialComm library &amp; jMBus library.<br>
 *
 *     jMBus -> https://www.openmuc.org/m-bus/
 *     jSerialComm -> https://fazecast.github.io/jSerialComm/
 */
public class Connector implements TransportLayer {

    SerialPort currentPort;

    public Connector(SerialPort currentPort) {
        this.currentPort = currentPort;
    }

    @Override
    public SerialPort getSerialPort() {
        return currentPort;
    }

    @Override
    public void open() {
        currentPort.openPort();
    }

    @Override
    public void close() {
        currentPort.closePort();
    }

    @Override
    public DataOutputStream getOutputStream() {
        return new DataOutputStream(currentPort.getOutputStream());
    }

    @Override
    public DataInputStream getInputStream() {
        return new DataInputStream(currentPort.getInputStream());
    }

    @Override
    public boolean isClosed() {
        return !currentPort.isOpen();
    }

    @Override
    public void setTimeout_in_ms(int timeout_in_ms) {
        currentPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, timeout_in_ms, Constants.STANDARD_WRITING_TIMEOUT_IN_MS.time_in_ms);
    }

    @Override
    public int getTimeout_in_ms() {
        return currentPort.getReadTimeout();
    }

}

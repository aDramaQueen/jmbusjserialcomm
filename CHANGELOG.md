- **1.2**
  - Updated dependencies
- **1.1**
    * Updated license
    * Simplified raw buffer return values from [VariableDataStructure](main/src/main/java/org/openmuc/jmbus/VariableDataStructure.java) 
- **1.0**
    * Finished migration from jRxTx to jSerialComm
    * Finished Gradle simplification
    * Finished README
- **0.3**
    * Simplified Gradle tasks
    * There are still 2 Projects: The root project "jmbusjserialcomm" & one inner project "cli".
- **0.2**
    * Cut all dependencies from [jRxTx](https://github.com/openmuc/jrxtx) & replace with [jSerialComm](https://github.com/Fazecast/jSerialComm)
- **0.0.2**
    * Added GitIgnore
    * Gradle integration
- **0.0.1**
    * Initialization of Project
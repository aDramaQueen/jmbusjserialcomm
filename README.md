# jMBusJSerialComm

Fork of jMBus library (3.3.0) optimized for [<ins>MBus Splitter</ins>](https://gitlab.com/aDramaQueen/mbussplitter) devices.
All older dependencies with [<ins>jRxTx library</ins>](https://github.com/openmuc/jrxtx) were replaced with [<ins>jSerialComm library</ins>](https://github.com/Fazecast/jSerialComm). This guaranties maximal compatibility to many systems.

## Purpose

If you don't want to generate native libraries for serial communication, that are needed for [jRxTx](https://github.com/openmuc/jrxtx) to work properly, but still do NOT want to spare the [jMBus](https://www.openmuc.org/m-bus/) library, then this is for you. All old dependencies to [jRxTx](https://github.com/openmuc/jrxtx) were replaced to the well maintained [jSerialComm](https://github.com/Fazecast/jSerialComm) library. Just replace your old jMbus JAR with this one & your good to go.

## Usage

There are 2 Gradle projects:
* The first project: **main**
    + This is the main project/library which gives MBus utility functions.
* The second project: **cli**
    + Some CLI options to start some simple operation, like read out a MBus device with specific primary address & baud rate.
* To check project structure, run following command:
    + `C:\...\path\to\project> gradlew projects`

+ To generate the main project JAR, run following command:
    * `C:\...\path\to\project> gradlew -p main clean build javadocjar`
    * The resulting JARs will be in directory: `C:\...\path\to\project\main\build\libs`

+ To generate the CLI project JAR, run following command:
    * `C:\...\path\to\project> gradlew -p cli clean build`
    * The resulting JAR will be in directory: `C:\...\path\to\project\cli\build\libs`


## Run CLI App

**<ins>ATTENTION:</ins>** The CLI depends on the main library. It will **NOT** work as a standalone application! It has to be loaded with "jmbusjserialcomm" library.

* Therefore, after you compiled **main** and **cli** JARs, copy & paste them to one directory of your choice.
* Open console/terminal in that directory.
* Enter following command for help text of CLI:

    `C:\...\path\to\collected\JARs>java -cp jmbusjserialcomm-1.0.jar;jmbusjserialcomm-1.0-cli.jar org.openmuc.jmbus.app.ConsoleApp --help`

## Requirements

* Java 17

## API

* [**jMBus**](https://www.openmuc.org/m-bus/javadoc/)
* [**jSerialComm**](https://fazecast.github.io/jSerialComm/javadoc/)

## [Change Log](CHANGELOG.md)